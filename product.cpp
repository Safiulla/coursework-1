#include "product.h"
#include <string>

/*
  product.cpp
  author: M00692387
  Created: 05/01/2021
  Last updated: 16/01/2021
*/

/* 
  Standard initialising constructor
  @param all attributes
*/
Product::Product( int id, double cost, int stock ) {
  this->id = id;
  this->cost = cost;
  this->stock = stock;
}

Product::Product(){}

int Product::getId(){ 
  return this->id;
}
double Product::getCost() {
  return this->cost;
}
int Product::getStock() {
  return this->stock;
}

void Product::setStock(int stock) {
  this->stock = stock;
}
void Product::setCost(double cost) {
  this->cost = cost;
}
void Product::setId(int id) {
  this->id = id;
}

/* Inherited classes 
  Standard initialising constructor
  @param all attributes
*/
Cd::Cd(std::string albumName, std::string artistName,
 std::string genre, std::string publisher, int id,
 double cost, int stock) : Product(id, cost, stock) {
  this->albumName = albumName;
  this->artistName = artistName;
  this->genre = genre;
  this->publisher = publisher;
}

// Reserve blank constructor
Cd::Cd() {}

std::string Cd::getName() {
  return this->albumName;
}
std::string Cd::getArtistName() {
  return this->artistName;
}
std::string Cd::getGenre() {
  return this->genre;
}
std::string Cd::getPublisher() {
  return this->publisher;
}


void Cd::setAlbumName(std::string albumName) {
  this->albumName = albumName;
}
void Cd::setArtistName(std::string artistName) {
  this->artistName = artistName;
}
void Cd::setGenre(std::string genre) {
  this->genre = genre;
}
void Cd::setPublisher(std::string publisher) {
  this->publisher = publisher;
}

/*
	toString function for writing to file/vector
	@param none
	@returns a string of all attributes in one line
*/
std::string Cd::toString(){
	std::string strid = std::to_string(this->getId());
	std::string strcost = std::to_string(this->getCost());
	std::string strstock = std::to_string(this->getStock());
	return (" ,"+this->albumName+","+this->artistName+","+this->genre+","
	+this->publisher+","+strid+","+strcost+","+strstock);
}

/* 
  Standard initialising constructor
  @param all attributes
*/
Dvd::Dvd(std::string albumName, std::string artistName,
 std::string genre, std::string publisher, int id,
 double cost, int stock) : Product(id, cost, stock) {
  this->albumName = albumName;
  this->artistName = artistName;
  this->genre = genre;
  this->publisher = publisher;
}

Dvd::Dvd() {}

std::string Dvd::getName() {
  return this->albumName;
}
std::string Dvd::getArtistName() {
  return this->artistName;
}
std::string Dvd::getGenre() {
  return this->genre;
}
std::string Dvd::getPublisher() {
  return this->publisher;
}


void Dvd::setAlbumName(std::string albumName) {
  this->albumName = albumName;
}
void Dvd::setArtistName(std::string artistName) {
  this->artistName = artistName;
}
void Dvd::setGenre(std::string genre) {
  this->genre = genre;
}
void Dvd::setPublisher(std::string publisher) {
  this->publisher = publisher;
}

/*
	toString function for writing to file/vector
	@param none
	@returns a string of all attributes in one line
*/
std::string Dvd::toString(){
	std::string strid = std::to_string(this->getId());
	std::string strcost = std::to_string(this->getCost());
	std::string strstock = std::to_string(this->getStock());
	return (" ,"+this->albumName+","+this->artistName+","+this->genre+","
	+this->publisher+","+strid+","+strcost+","+strstock);
}

/* 
  Standard initialising constructor
  @param all attributes
*/
Book::Book(std::string title, std::string author,
 std::string ISBN, std::string publisher, std::string genre,
 int id, double cost, int stock) : Product(id, cost, stock) {
  this->title = title;
  this->author = author;
  this->ISBN = ISBN;
  this->publisher = publisher;
  this->genre = genre;
}

// Reserve Blank Constructor
Book::Book() {}

std::string Book::getName() {
  return this->title;
}
std::string Book::getAuthor() {
  return this->author;
}
std::string Book::getISBN() {
  return this->ISBN;
}
std::string Book::getPublisher() {
  return this->publisher;
}
std::string Book::getGenre() {
  return this->genre;
}

void Book::setTitle(std::string title) {
  this->title = title;
}
void Book::setAuthor(std::string author) {
  this->author = author;
}
void Book::setISBN(std::string ISBN) {
  this->ISBN = ISBN;
}
void Book::setPublisher(std::string publisher) {
  this->publisher = publisher;
}
void Book::setGenre(std::string genre) {
  this->genre = genre;
}

/*
	toString function for writing to file/vector
	@param none
	@returns a string of all attributes in one line
*/
std::string Book::toString(){
	std::string strid = std::to_string(this->getId());
	std::string strcost = std::to_string(this->getCost());
	std::string strstock = std::to_string(this->getStock());
	return (" ,"+this->title+","+this->author+","+this->ISBN+","+this->publisher
	+","+this->genre+","+strid+","+strcost+","+strstock);
}

/* 
  Standard initialising constructor
  @param all attributes
*/
Magazine::Magazine(std::string title, std::string publisher,
 std::string ISSN, std::string volumeIssue, std::string genre,
 int id, double cost, int stock) : Product(id, cost, stock) {
  this->title = title;
  this->publisher = publisher;
  this->ISSN = ISSN;
  this->volumeIssue = volumeIssue;
  this->genre = genre;
}

// Reserve Blank Constructor
Magazine::Magazine() {}

std::string Magazine::getName() {
  return this->title;
}
std::string Magazine::getPublisher() {
  return this->publisher;
}
std::string Magazine::getISSN() {
  return this->ISSN;
}
std::string Magazine::getVolumeIssue() {
  return this->volumeIssue;
}
std::string Magazine::getGenre() {
  return this->genre;
}

void Magazine::setTitle(std::string title) {
  this->title = title;
}
void Magazine::setPublisher(std::string publisher) {
  this->publisher = publisher;
}
void Magazine::setISSN(std::string ISSN) {
  this->ISSN = ISSN;
}
void Magazine::setVolumeIssue(std::string volumeIssue) {
  this->volumeIssue = volumeIssue;
}
void Magazine::setGenre(std::string genre) {
  this->genre = genre;
}

/*
	toString function for writing to file/vector
	@param none
	@returns a string of all attributes in one line
*/
std::string Magazine::toString(){
	std::string strid = std::to_string(this->getId());
	std::string strcost = std::to_string(this->getCost());
	std::string strstock = std::to_string(this->getStock());
	return (" ,"+this->title+","+this->publisher+","+this->ISSN+","
	+this->volumeIssue+","+this->genre+","+strid+","+strcost+","+strstock);
}
