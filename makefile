CC = g++ 

.PHONY: clean remove

all: run clean

run: output
	./output

testset: testcase remove

testcase: test
	./test

test: testing.o product.o
	$(CC) testing.o product.o -o test

testing.o: testing.cpp
	$(CC) -c testing.cpp

output: CW1.o product.o
	$(CC) CW1.o product.o -o output

CW1.o: CW1.cpp
	$(CC) -c CW1.cpp

product.o: product.cpp product.h
	$(CC) -c product.cpp

remove:
	rm *.o test

clean:
	rm *.o output